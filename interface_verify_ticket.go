package wechat_sdk_go

type VerifyTicketInterface interface {
	Get(appid string) string
}
