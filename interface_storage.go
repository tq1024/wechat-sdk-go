package wechat_sdk_go

type StorageInterface interface {
	Get(key interface{}) map[string]interface{}
	Add(key interface{}, values interface{}) bool
	Save(key interface{}, values interface{}) bool
	Delete(key interface{}) bool
}
