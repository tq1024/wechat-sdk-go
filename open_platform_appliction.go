package wechat_sdk_go

type OpenPlatform struct {
	request      *BaseRequest
	verifyTicket VerifyTicketInterface
	appid        string
	appsecret    string
}

func NewOpenPlatform(appid string, appsecret string, verifyTicket VerifyTicketInterface) *OpenPlatform {
	accessToken := NewComponentAccessToken(appid, appsecret, verifyTicket)

	r := NewBaseRequest()
	r.AccessToken = accessToken

	op := OpenPlatform{
		r,
		verifyTicket,
		appid,
		appsecret,
	}

	return &op
}

func (op *OpenPlatform) GetAccessToken() (*Token, error) {
	return op.request.AccessToken.GetToken()
}
