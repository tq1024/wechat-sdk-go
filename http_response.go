package wechat_sdk_go

import (
	"io/ioutil"
	"net/http"
)

type Response struct {
	*http.Response
}

func (rsp *Response) GetBody() ([]byte, error) {

	var err error

	defer func() {
		err = rsp.Body.Close()
	}()

	body, err := ioutil.ReadAll(rsp.Body)

	if nil != err {
		return nil, err
	}

	return body, err

}
